<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Put short content here">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Contact Us Now</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/contactus.jfif);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Contact Us</h2>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->
	
    <section class="contact-our-area section-padding-100-0">
        <div class="container">
            <div class="row" id="contact">
                <!-- Heading -->
                <div class="col-12">
                    <div class="section-heading-2 text-center wow fadeInDown" data-wow-delay="300ms">
                        <h4>WOULD YOU LIKE TO KNOW MORE?</h4>
                        <p>If you have any enquiry, feedback and suggestion, don't hesitate to send to us.</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-between">
                <div class="col-12 col-sm-3 wow fadeInLeft" data-wow-delay="300ms">
                    <div class="contact-information mb-100">
                        <!-- Single Contact Info -->
                        <div class="single-contact-info">
                            <p>Address:</p>
                            <h6>Hotspur Sdn. Bhd., No 10, Jalan 5/10F, Seksyen 13, 43600 Bandar Baru Bangi, Selangor.</h6>
                        </div>
                        <!-- Single Contact Info -->
                        <div class="single-contact-info">
                            <p>Phone:</p>
                            <h6>0123456789</h6>
                        </div>
                        <!-- Single Contact Info -->
                        <div class="single-contact-info">
                            <p>Email:</p>
                            <a href=""><h6>info@gmail.my</h6></a>
                        </div>
                        <!-- Single Contact Info -->
                        <div class="single-contact-info">
                            <p>Website:</p>
                            <a href=""><h6>www.bandarbarubangi.gmail.my</h6></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-8 wow fadeInRight" data-wow-delay="300ms">
                    <!-- Contact Form -->
                    <div class="contact_from_area mb-100 clearfix">
                        <div class="contact_form">
                            <form action="mail_contact_us.php" method="post" id="main_contact_form">
                                <div class="contact_input_area">
                                    <div id="success_fail_info"></div>
                                    <div class="row">
                                        <!-- Form Group -->
										
                                        <div class="col-12 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Your Name" required>
                                            </div>
                                        </div>
                                        <!-- Form Group -->
                                        <div class="col-12 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control mb-30" name="lname" id="lname" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <!-- Form Group -->
                                        <div class="col-12 col-lg-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                            </div>
                                        </div>
                                        <!-- Form Group -->
                                        <div class="col-12 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control mb-30" name="mobile" id="mobile" placeholder="Contact Number" required>
                                            </div>
                                        </div>
                                        <!-- Form Group -->
                                        <div class="col-12">
                                            <div class="form-group">
                                                <textarea name="message" class="form-control mb-30" id="message" cols="30" rows="6" placeholder="Your Message *" required></textarea>
                                            </div>
                                        </div>
										
										<div class="col-12 hidden">
                                            <div class="form-group">
                                                <input type="text" class="form-control mb-30" name="myemail" id="myemail" placeholder="My Email Dummy">
                                            </div>
                                        </div>
										
                                        <!-- Button -->
                                        <div class="col-12">
                                            <button type="submit" class="btn confer-btn">Send Message</button>
                                        </div>
										
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Map Area -->
    <div class="map-area">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63752.2474419162!2d101.72657778917753!3d2.954364449139751!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdcbd32f300b1d%3A0xbc8b1c2b2792e85e!2sBandar%20Baru%20Bangi%2C%20Selangor!5e0!3m2!1sen!2smy!4v1622640044631!5m2!1sen!2smy" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <!-- Contact Info Area -->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact--info-area bg-boxshadow">
                    <div class="row">
                        <!-- Single Contact Info -->
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="single-contact--info text-center">
                                <!-- Contact Info Icon -->
                                <div class="contact--info-icon">
                                    <img src="img/core-img/icon-5.png" alt="">
                                </div>
								</br>
                                <h5>Hotspur Sdn. Bhd., No 10, Jalan 5/10F, Seksyen 13, 43600 Bandar Baru Bangi, Selangor.</h5>
                            </div>
                        </div>

                        <!-- Single Contact Info -->
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="single-contact--info text-center">
                                <!-- Contact Info Icon -->
                                <div class="contact--info-icon">
                                    <img src="img/core-img/icon-6.png" alt="">
                                </div>
								</br>
                                <h5>0123456789</h5>
                            </div>
                        </div>

                        <!-- Single Contact Info -->
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="single-contact--info text-center">
                                <!-- Contact Info Icon -->
                                <div class="contact--info-icon">
                                    <img src="img/core-img/icon-7.png" alt="">
                                </div>
								</br>
                                <h5>info@gmail.my</h5>
                            </div>
                        </div>

                        <!-- Single Contact Info -->
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="single-contact--info text-center">
                                <!-- Contact Info Icon -->
                                <div class="contact--info-icon">
                                    <img src="img/core-img/icon-8.png" alt="">
                                </div>
								</br>
                                <h5>www.bandarbarubangi.gmail.my</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>