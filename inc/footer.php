<footer class="footer-area bg-img bg-overlay-2 section-padding-30-0">
        <!-- Main Footer Area -->
        <div class="mobilc main-footer-area">
            <div class="container">
                <div class="row">
                    <!-- Single Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-footer-widget mb-60">
                            <!-- Footer Logo -->
                            <a href="#" class="footer-logo"><img src="img/home.png" alt=""></a><br>
                            <a href="#" class="footer-logo"><img src="img/2.png" alt=""></a>
                        </div>
                    </div>

                    <!-- Single Footer Widget Area -->
                    <div class="col-12 col-sm-12 col-lg-3">
                        <div class="single-footer-widget mb-10">
                            <!-- Widget Title -->
                            <h5 class="widget-title">Contact Us</h5>

                            <!-- Contact Area -->
                            <div class="footer-contact-info">
                                <p><i class="zmdi zmdi-pin"></i> Hotspur Sdn. Bhd., No 10, Jalan 5/10F, Seksyen 13, 43600 Bandar Baru Bangi, Selangor.</p>
                                <p><i class="zmdi zmdi-phone"></i> 0123456789</p>
                                <p><i class="zmdi zmdi-email"></i> info@gmail.my</p>
                                <p><i class="zmdi zmdi-time"></i> 8:30 AM - 5:30 PM MON - FRI</p>
                            </div>
                        </div>
                    </div>

                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="single-footer-widget mb-60">
                                <!-- Widget Title -->
                                <h5 class="widget-title">Follow Us</h5>

                                <!-- Social Info -->
                                <div class="social-info">
                                    <a href="" ><i class="zmdi zmdi-facebook"></i></a>
                                    <a href="" ><i class="zmdi zmdi-twitter"></i></a>
                                    <a href="" ><i class="zmdi zmdi-instagram"></i></a>                              
                                    <a href="" ><i class="zmdi zmdi-linkedin"></i></a>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>

        <!-- Copywrite Area -->
        <div class="container">
            <div class="copywrite-content">
                <div class="row">
                    <!-- Copywrite Text -->
                    <div class="col-12 col-md-12">
                        <div class="copywrite-text">
                            <p><i class="fa fa-copyright"></i> Copyright 2020 | Hotspur Sdn. Bhd. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>