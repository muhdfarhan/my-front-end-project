<header class="header-area">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="conferNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="./index.php"><img width="300px" src="./img/home.png" alt=""></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">
                        <!-- Menu Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>
                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
								<li><a href="index.php">Home</a></li>
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="core-business.php">Core Business</a>
                                    <ul class="dropdown">
                                        <li><a href="electrical.php" target="_parent">Electrical</a></li>
                                        <li><a href="mechanical.php" target="_parent">Mechanical</a></li>
                                        <li><a href="medical.php" target="_parent">Medical</a></li>
                                        <li><a href="lift-escalator.php" target="_parent">Lift</a></li>
                                        <li><a href="railway.php" target="_parent">Railway</a></li>
                                        <li><a href="training.php" target="_parent">Equipment</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.php">Contact Us</a></li>
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>