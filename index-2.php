<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Put short content here">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/logo5.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Welcome Area Start -->
    <section class="welcome-area">
        <div class="welcome-slides owl-carousel">
            
            <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(img/hotspur-img/2.jpeg);" data-stellar-background-ratio="0.2">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <!-- Welcome Text -->
                        <div class="col-12">
                            <div class="mobilc welcome-text-two text-left">
                                </br>
								</br>
								<h5 data-animation="fadeInUp" data-delay="100ms">SUPERIOR TURNKEY SOLUTIONS FOR ENGINEERING SERVICES</h5>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="welcome-slides owl-carousel">
            
            <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(img/hotspur-img/1.jpeg);" data-stellar-background-ratio="0.2">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <!-- Welcome Text -->
                        <div class="col-12">
                            <div class="mobilc welcome-text-two text-left">
                                </br>
								</br>
								<h5 data-animation="fadeInUp" data-delay="100ms">Accelerating Innovation</h5>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="welcome-slides owl-carousel">
            
            <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(img/hotspur-img/3.jpeg);" data-stellar-background-ratio="0.2">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <!-- Welcome Text -->
                        <div class="col-12">
                            <div class="mobilc welcome-text-two text-left">
                                </br>
								</br>
								<h5 data-animation="fadeInUp" data-delay="100ms">Malaysia's Renowned M&E Engineering Contractor since 1992</h5>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll Icon -->
        <div class="icon-scroll" id="scrollDown"></div>
    </section>

	<section class="our-speaker-area section-padding-30-0">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="section-heading text-center">
						<h3>About Hotspur</h3>
						<p>Established in 1991, Hotspur embarked its journey in the engineering services sector as a government contractor in the areas of mechanical & electrical (M&E)
						engineering, civil engineering, building and maintenance services, training systems, software and equipment supply. 27 years fast forwarded and over RM500
						million projects later, Hotspur has evolved significantly in mult i folds and cemented its strong reputation as one of the longest standing Bumiputra Mechanical and
						Electrical Engineering (M&E) contractors in the market providing full-fledge services for Hot Water System, Air Conditioning & Mechanical Ventilation (ACMV), Lift
						& Escalators, Medical Gas, Fire Fighting System and Building Management System among others.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- End brands Area-->
    <!-- About Us And Countdown Area Start -->
    <section class="about-us-countdown-area section-padding-115-20" id="about">
        <div class="container">
            <div class="row align-items-center">
			<!-- Heading -->
                <div class="col-12">
                    <div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
                        <h4>Our Mission</h4>
                        
                    </div>
                </div>
                <div class="row">
					<div class="col-12">
						<div class="mobilt mb-80 ml-100 mr-100 wow fadeInRight" data-wow-delay="300ms">
						<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Partnership to provide value added solutions for client</p>
						<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;The cornerstone of our previous projects is that wevalue the partnership with our associates and suppliers in order to consistently provide value added solutions for clients.</p>						
						<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Hotspur seeks to build bridges of mutual cooperation that foster the sharing of knowledge and skills in ensuring the continued success of its corporate goals.</p>
						</div>
					</div>
				</div>

				<div class="col-6">
                    <div class="section-heading-3 text-center wow fadeInLeft" data-wow-delay="300ms">
                        <h4>ABCDEF Policy</h4>
                    </div>
				</div>
				<div class="col-6">	
					<div class="section-heading-3 text-center wow fadeInRight" data-wow-delay="300ms">
                        <h4>Quality Policy</h4>
                    </div>
                </div>

                <div class="row">
					<div class="col-6">
						<div class="mobilt mb-80 ml-100 wow fadeInLeft" data-wow-delay="300ms">
							<p>Governed by the ABCDEF principles introduced by our founder YM Datuk Seri Utama Raja Nong Chik, Hotspur benchmarks its working standards internally to deliver the penultimate satisfaction to all of its clients.</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;A – Amanah – Integrity</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;B – Berdisiplin – Discipline</p>	
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;C – Cekap – Skilled</p>	
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;D – Dedikasi – Dedicated</p>	
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;E – Erat – Relationship</p>	
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;F – Finishing</p>	
						</div>
					</div>
					<div class="col-6">
						<div class="mobilt mb-80 ml-100 wow fadeInRight" data-wow-delay="300ms">
							<p>Quality is a fundamental part of Hotspur’s working culture and underscores our ethical responsibility and accountability to both stakeholders and clients alike. These principles helps to guide our actions to deliver products and services that are safe, compliant and preferred. It is our utmost pleasure to commit to never compromise on the safety, compliance and quality of services, regardless of the circumstances.</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;R – Reliable in performance and meeting targets</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;A – Adequate resources to support the QMS</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;S – Soaring progress to our valuable stakeholders i.e. employees, supplier, subcontractor, clients and shareholders</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;M – Make sure Quality delivery and Maintenance to customers</p>
							<p><i class="fa fa-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;A – Achieved the quality objectives set and complete the project according to schedule</p>
						</div>
					</div>
				</div>
			
            </div>
        </div>
		
    </section>
	</br>
	</br>
    <!-- About Us And Countdown Area End -->
	
	<section class="section-padding-70-20" id="page">
        <div>
            <div class="row align-items-center">
				<div id="colorlib-intro"  class="intro-img jarallax" style="background-image: url(img/bg-img/default2.jpg);" data-stellar-background-ratio="0.5">
					<div class="overlay"></div>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="section-heading-3 text-center wow fadeInUp" data-wow-delay="300ms">
									<h5>Our Core Business</h5>
								</div>

								<div class="row">

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="200">
									<div class="block-2"> <!-- .hover -->
										<div class="flipper">
										<div class="front" style="background-image: url(images/p5.jpg);">
											<div class="box">
											<h2>Electrical</h2>
											
											</div>
										</div>
										<div class="back">
											<!-- back content -->
											<blockquote>
											<p>Electrical Engineering</p>
											</blockquote>
											
										</div>
										</div>
									</div> <!-- .flip-container -->
									</div>

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="100">
								<div class="block-2">
									<div class="flipper">
									<div class="front" style="background-image: url(images/p13.jpg);">
										<div class="box">
										<h2>Mechanical</h2>
										
										</div>
									</div>
									<div class="back">
										<!-- back content -->
										<blockquote>
										<p>Mechy Engineering</p>
										</blockquote>
										
									</div>
									</div>
								</div> <!-- .flip-container -->
								</div>

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="100">
									<div class="block-2">
										<div class="flipper">
										<div class="front" style="background-image: url(images/p7.jpg);">
											<div class="box">
											<h2>Medical Engineering</h2>
											
											</div>
										</div>
										<div class="back">
											<!-- back content -->
											<blockquote>
											<p>Medical Purposes</p>
											</blockquote>
											
										</div>
										</div>
									</div> <!-- .flip-container -->
								</div>

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="200">
								<div class="block-2"> <!-- .hover -->
									<div class="flipper">
									<div class="front" style="background-image: url(images/p8.jpg);">
										<div class="box">
										<h2>Lift Escalator</h2>
										
										</div>
									</div>
									<div class="back">
										<!-- back content -->
										<blockquote>
										<p>Building Material</p>
										</blockquote>
										
									</div>
									</div>
								</div> <!-- .flip-container -->
								</div>

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="100">
								<div class="block-2">
									<div class="flipper">
									<div class="front" style="background-image: url(images/p10.jpg);">
										<div class="box">
										<h2>Railway</h2>
										
										</div>
									</div>
									<div class="back">
										<!-- back content -->
										<blockquote>
										<p>Train</p>
										</blockquote>
										
									</div>
									</div>
								</div> <!-- .flip-container -->
								</div>

								<div class="col-md-12 col-lg-2" data-aos="fade-up" data-aos-delay="300">
									<div class="block-2">
										<div class="flipper">
										<div class="front" style="background-image: url(images/p6.jpg);">
											<div class="box">
											<h2>Training Equipment</h2>
											
											</div>
										</div>
										<div class="back" >
											<!-- back content -->
											<blockquote>
											<a href="training.php" ><p>For Training</p></a>
											</blockquote>
											
										</div>
										</div>
									</div> <!-- .flip-container -->
								</div>

								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
        </div>   
    </section>
	
    <!-- Our Client Area -->

	<section class="brands-area">
		        
		<div id="clients">
		
			<div class="clients-wrap">
			<ul id="clients-list" class="clearfix">
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p1.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p2.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p3.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p4.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p5.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p6.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p7.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p8.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p9.png" alt=""></a>
				</li>
				<li>
				<a href=""><img width="100px" src="img/hotspur-img/p10.png" alt=""></a>
				</li>
				
			</ul>
			</div>
			<!-- end clients-wrap -->
		</div>
	</section>
	</br>
	<p align="center">OUR PARTNERS & ASSOCIATES</p>
	</br>

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->
	
    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
	<script src="js/magnific-popup-options.js"></script>
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>
	<!-- Slider -->
	
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="js/jquery.hislide.js" ></script>
		<script>
			$('.slide').hiSlide();
		</script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        var captchaChecked = function(){
            $('.btn-submit-queries').prop('disabled', false);
        }
        var captchaExpired = function(){
            $('.btn-submit-queries').prop('disabled', true);
        }
        var captchaError = function(){
            $('.btn-submit-queries').prop('disabled', true);
        }
	</script>
	
	<script>
		var coll = document.getElementsByClassName("collapsible");
		var i;

		for (i = 0; i < coll.length; i++) {
		coll[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var content = this.nextElementSibling;
			if (content.style.maxHeight){
			content.style.maxHeight = null;
			} else {
			content.style.maxHeight = content.scrollHeight + "px";
			} 
		});
		}
	</script>
</body>

</html>