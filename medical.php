<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Learn and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business | Medical Engineering</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/hotspur-img/medical.jpeg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Medical Engineering</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Area Start -->
    <section class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                            <!-- Post Thumbnail -->
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m0.jpeg" alt="">
                            </div>

                            <!-- Post Title -->
                            <h4 class="post-title">The solutions and the devices to enable medical professionals deliver quality healthcare</h4>

                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date"><i class="zmdi zmdi-alarm-check"></i> 2020</a>
                                <a class="post-author" href="" ><i class="zmdi zmdi-account"></i> Hotspur Sdn Bhd</a>
                                
                            </div>

                            <p>Medical engineering is the application of engineering principles and design concepts to medicine and biology for healthcare purposes</p>

                            <!-- Blockquote -->
                            <blockquote class="confer-blockquote">
                                <h5>Why Medical Engineering?</h5>
                            </blockquote>

                            </br>

                            <h4>Medical Gas Supply</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m1.jpg" alt="">
                            </div>                  
                            <p>Our commitment to engineering innovative solutions for our medical gas systems has also paved way for development of new value-adding products that translates to time and cost savings and improved user experience and safety.</p>

                            <br>

                            <h4>Medical Equipment</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m2.jpg" alt="">
                            </div>
                            <p>Hotspur also branched its portfolio by partaking in projects for Malaysia’s most reputable government and private Hospitals for the benefit of both patients and medical professionals. Currently Hotspur is serving Kementerian Kesihatan Malaysia (KKM) and KPJ Hospitals as its clients. It’s other clientele includes Hospital Bainun Ipoh dan KPJ Sg Penchala. Hotspur offers an extensive variety of medical equipment and supplies.</p>
                            <p>1. Incontinence Supplies</p>
                            <p>2. Compression Garments</p>
                            <p>3. Mobility devices (walkers, wheelchairs, etc.)</p>
                            <p>4. Orthopedic Braces and Supports</p>
                            <p>5. Wound care supplies</p>
                            <p>6. And much more!</p>

							</br>
							
                            <h4>Sterilizer</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m3.jpg" alt="">
                            </div>
                            <p>With a wealth of experience providing sterilization for medical devices, Hotspur has been providing a first-class sterilization service in medical industry. The sterilization of some products can be a complex and delicate process that requires the highest quality standards.</p>

                            <br>

                            <h4>Cleanroom and Environmental Room</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m4.jpg" alt="">
                            </div>
                            <p>Cleanroom and environmental rooms are required for a wide variety of sensitive products, and have a wide variety of specific cleaning requirements in order to comply with industry regulations.</p>

                            <br>

							<h4>OT Light, OT Tables and OT Equipment</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m5.jpg" alt="">
                            </div>
                            <p>We pave the way for technological breakthroughs by offering reliable equipment and excellent service.</p>
                            
                            <br>

                            <h4>Angiography machine</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m6.jpg" alt="">
                            </div>
                            <p>Come with experience in personalization to put you in control and reduce manual tasks. Your personal preferences and protocols are pre-programmed saving time and effort. Everything from patient data management, exam scheduling, image acquisition, system movement, image post-processing and archiving can be set to match each individual clinician’s preferred way of working.</p>

                            <br>

                            <h4>X RAY MACHINE AND RELATED EQUIPMENT</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/m1.jpg" alt="">
                            </div>
                            <p>Hotspur provides a broad array of Line Frequency and High Frequency X-Ray Systems from Portable to High Powered. Each Equipment is carefully designed to assure the utmost accuracy and reliability. Our equipment has been designed keeping in mind PRECISION, the prime factor for accurate X-Ray dosage, nil stray radiation & TRUE Image quality. Our X-Ray System reflects Hotspur commitment to excellent in clinical performance & economic value.</p>

						</div>

                        <!-- Post Tags & Social Info -->
                        <div class="post-tags-social-area mt-30 pb-5 d-flex flex-wrap align-items-center">
                            <!-- Popular Tags -->
                            <div class="popular-tags d-flex align-items-center">
                                <p><i class="zmdi zmdi-label"></i></p>
                                <ul class="nav">
                                    <li><a href="#">Medical Engineering</a></li>
                                </ul>
                            </div>

                            <!-- Author Social Info -->
                            <div class="author-social-info">
                                <a href="" ><i class="zmdi zmdi-facebook"></i></a>
								<a href="" ><i class="zmdi zmdi-linkedin"></i></a>                               
                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                            </div>
                        </div>

                        

                        
						</br>
                        <!-- Leave A Reply 
                        <div class="confer-leave-a-reply-form clearfix">
                            <h4 class="mb-30">Leave A Comment</h4>
                            
                            <!-- Leave A Reply 
                            <div class="contact_form">
                                <form action="#" method="post">
                                    <div class="contact_input_area">
                                        <div class="row">
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Your Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name-2" placeholder="Last Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="subject" id="subject" placeholder="Your Number">
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea name="message" class="form-control mb-30" id="message" cols="30" rows="6" placeholder="Message" required></textarea>
                                                </div>
                                            </div>
                                            <!-- Button 
                                            <div class="col-12">
                                                <button type="submit" class="btn confer-btn">Send Message <i class="zmdi zmdi-long-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>-->
                    </div>
                </div>

                
            </div>
        </div>
    </section>
    <!-- Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>