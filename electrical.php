<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Learn and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business | Electrical Engineering</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/hotspur-img/me12.jpeg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Electrical Engineering</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Area Start -->
    <section class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                            <!-- Post Thumbnail -->
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me1.jpeg" alt="">
                            </div>

                            <!-- Post Title -->
                            <h4 class="post-title">Specifically encompasses the in-depth design and selection of these systems, as opposed to a tradesperson simply installing equipment. </h4>

                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date"><i class="zmdi zmdi-alarm-check"></i> 2020</a>
                                <a class="post-author" href="" ><i class="zmdi zmdi-account"></i> Hotspur Sdn Bhd</a>
                                
                            </div>

                            <p>Hotspur’s operation in M&E services started in year 1991. The commitment, loyalty and passion given by the support staff has earned Hotspur its reputation as one the most sturdy dynamic Bumiputra M&E G7 contractor in Malaysia.</p>

                            <!-- Blockquote -->
                            <blockquote class="confer-blockquote">
                                <h5>Core Engineering?</h5>
                            </blockquote>

                            </br>

                            <h4>Heating, Ventilating and Air-Conditioning (HVAC) Installation and Services</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me2.jpg" alt="">
                            </div>                  
                            <p>Hotspur is a specialist and committed to provide innovative air-conditioning and ventilation solutions to meet the ever-changing demands of the Malaysian market. This includes the supply, installation and maintenance of air-conditioning and mechanical ventilation system that is certified safe by regulatory councils and boards</p>
                            <p>1. Water cooled chiller system</p>
                            <p>2. Air cooled chiller system</p>
                            <p>3. Variable refrigerant volume system (“VRV”)</p>
                            <p>4. Water cooled package system</p>
                            <p>5. Air cooled package system</p>
                            <p>6. District cooling system</p>
                            <p>7. Mechanical ventilation systems</p>
                            <p>8. Air cooled split unit</p>
                            <p>9. Gas heat pump air-conditioning systems</p>
                            <br>

                            <h4>Fire Fighting and Protection Services</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me3.jpg" alt="">
                            </div>
                            <p>Backed by an uncompromising commitment to our customers, communities and the environment we all live in, Hotspur is dedicated to provide its customers with a full range of services for fire fighting systems to ensure heightened precautionary and crisis management systems in an unforessen fire situation.</p>
                            <p>1. Automatic Sprinkler System</p>
                            <p>2. Breakglass and Alarm Bell System</p>
                            <p>3. Fire and Smoke Detection System</p>
                            <p>4. Carbon Dioxide Extinguishing System</p>
                            <p>5. Water Spray and Deluge Form System</p>
                            <p>6. Wet and Dry Riser System</p>
                            <p>7. Smoke and Heat Detectors</p>

							</br>
							
                            <h4>Building Management System (BMS)</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me4.jpeg" alt="">
                            </div>
                            <p>A building management system (BMS) is a control system that can be used to monitor and manage the mechanical, electrical and electromechanical services in a facility. Such services can include power, heating, ventilation, air-conditioning, physical access control, pumping stations, elevators and lights</p>

                            <br>

                            <h4>Sanitary and Plumbing Services</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me5.jpeg" alt="">
                            </div>
                            <p>We can handle those plumbing needs and more, from installation, cleaning, and maintenance to full repair services for virtually any plumbing-related product.</p>
                            <p>1. Cold, hot water supply and distribution</p>
                            <p>2. Sanitary plumbing system</p>
                            <p>3. Rain water down pipe system</p>
                            <p>4. Sewerage pipe and man hole drainage</p>
                            <br>

							<h4>Air Compressor System</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me6.jfif" alt="">
                            </div>
                            <p>Hotspur professional and dedicated team provide a turnkey solution to each customer’s requirements through consultancy, supply, service and install air compressors, compressed air equipment, high pressure testing equipment and complete airline systems.</p>
                            
                            <br>

                            <h4>Overhead Crane</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me7.jpeg" alt="">
                            </div>
                            <p>We provide an extensive range of cranes, for light duty applications and demanding processes. Our standard offering with high quality basic equipment or advanced offering with high-tech features.</p>

                            <br>

                            <h4>Bore Evacuator or Fume Extractor</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me8.jpg" alt="">
                            </div>
                            <p>Hotspur team of experts to assist you with all your fume extractor requirements. We can provide solutions for most demanding challenges. Whether you have large production line operations or a unique manufacturing process, we can develop a welding smoke extraction system to meet your needs.</p>

                            <h4>Waste Automation System</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me13.png" alt="">
                            </div>
                            <p>Along with its technology partner provides leading pneumatic technologies and smart design engineering that increases a facility’s efficiency and improves cleanliness and aesthetics.</p>
                            
                            <br>

                            <h4>Electrical Services</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me10.jpeg" alt="">
                            </div>
                            <p>Hotspur offers a diverse range of electrical services including for industrial, domestic and commercial. The team committed and capable of making sure all projects are completed on time and on budget.</p>
                            <p>1. Lighting</p>
                            <p>2. Switchboard Upgrade</p>
                            <p>3. Alarm System</p>
                            <p>4. Cable and Management Control</p>
                            <br>

                            <h4>Pneumatic Tube System</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/me9.jpg" alt="">
                            </div>
                            <p>Pneumatic Tube are system that propel cylindrical containers through networks of tubes by air or partial vacuum.</p>

						</div>

                        <!-- Post Tags & Social Info -->
                        <div class="post-tags-social-area mt-30 pb-5 d-flex flex-wrap align-items-center">
                            <!-- Popular Tags -->
                            <div class="popular-tags d-flex align-items-center">
                                <p><i class="zmdi zmdi-label"></i></p>
                                <ul class="nav">
                                    <li><a href="#">Electrical Engineering</a></li>
                                </ul>
                            </div>

                            <!-- Author Social Info -->
                            <div class="author-social-info">
                                <a href="" ><i class="zmdi zmdi-facebook"></i></a>
								<a href="" ><i class="zmdi zmdi-linkedin"></i></a>                               
                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                            </div>
                        </div>
						</br>
                    </div>
                </div>

                
            </div>
        </div>
    </section>
    <!-- Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>