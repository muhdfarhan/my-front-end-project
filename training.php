<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Learn and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business | Training Equipment</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/hotspur-img/medical.jpeg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Training Equipment</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Area Start -->
    <section class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                            <!-- Post Thumbnail -->
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/te1.jpeg" alt="">
                            </div>

                            <!-- Post Title -->
                            <h4 class="post-title">TE</h4>

                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date"><i class="zmdi zmdi-alarm-check"></i> 2020</a>
                                <a class="post-author" href="" ><i class="zmdi zmdi-account"></i> Hotspur Sdn Bhd</a>
                                
                            </div>

                            <p>At Hotspur we have uniquely aligned ourselves with the needs of vocational technical schools, Polytechnics, and Universities around in Malaysia. We believe training equipment should be practically oriented and incorporate real components and parts that are found in the industries where students will be employed. We offer complete solutions for equipping technology workshops in each of our product areas including the transfer of knowledge.</p>

                            <!-- Blockquote -->
                            <blockquote class="confer-blockquote">
                                <h5>Training Equipment</h5>
                            </blockquote>

                            </br>

                            <h4>Our Product Areas</h4>                  
                            <p>1. Automotive Technology</p>
                            <p>2. Marine Technology</p>
                            <p>3. Mechanical Technology</p>
                            <p>4. E&E Technology</p>
                            <p>5. Hospitality</p>
                            <p>6. Textile Technology</p>
                            <p>7. Personnel</p>
                            <p>8. Photography Technology</p>
                            <p>9. Information Technology</p>
                            <p>10. Sports Technology</p>
                            <p>11. Oil and Gas</p>
                            <br>
						</div>

                        <!-- Post Tags & Social Info -->
                        <div class="post-tags-social-area mt-30 pb-5 d-flex flex-wrap align-items-center">
                            <!-- Popular Tags -->
                            <div class="popular-tags d-flex align-items-center">
                                <p><i class="zmdi zmdi-label"></i></p>
                                <ul class="nav">
                                    <li><a href="#">Training Equipment</a></li>
                                </ul>
                            </div>

                            <!-- Author Social Info -->
                            <div class="author-social-info">
                                <a href="" ><i class="zmdi zmdi-facebook"></i></a>
								<a href="" ><i class="zmdi zmdi-linkedin"></i></a>                               
                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                            </div>
                        </div>

                        

                        
						</br>
                        <!-- Leave A Reply 
                        <div class="confer-leave-a-reply-form clearfix">
                            <h4 class="mb-30">Leave A Comment</h4>
                            
                            <!-- Leave A Reply 
                            <div class="contact_form">
                                <form action="#" method="post">
                                    <div class="contact_input_area">
                                        <div class="row">
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Your Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name-2" placeholder="Last Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="subject" id="subject" placeholder="Your Number">
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea name="message" class="form-control mb-30" id="message" cols="30" rows="6" placeholder="Message" required></textarea>
                                                </div>
                                            </div>
                                            <!-- Button 
                                            <div class="col-12">
                                                <button type="submit" class="btn confer-btn">Send Message <i class="zmdi zmdi-long-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>-->
                    </div>
                </div>

                
            </div>
        </div>
    </section>
    <!-- Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>