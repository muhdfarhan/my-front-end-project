<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Learn and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business | Lift & Escalator</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/hotspur-img/lift.jpeg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Lift & Escalator</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Area Start -->
    <section class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                            <!-- Post Thumbnail -->
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/e0.jfif" alt="">
                            </div>

                            <!-- Post Title -->
                            <h4 class="post-title">Moving platform that help us move from one floor to another floor</h4>

                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date"><i class="zmdi zmdi-alarm-check"></i> 2020</a>
                                <a class="post-author" href="" ><i class="zmdi zmdi-account"></i> Hotspur Sdn Bhd</a>
                                
                            </div>

                            <p>To further ensure the quality of service Hotspur has expanded its lift division by taking Sword Elevator (M) Sdn Bhd under its wings in 2015. Sword Elevator (M) Sdn Bhd has established an exclusive agency agreement with Sword China, one of the largest local lift manufactures based in Hangzhou, China which manufactures about 40,000 lifts annually.</p>
                            <p>Shortly after the partnership establishment, Hotspur subsequently undertook lift installation projects for both commercial and residential projects in full force. This includes the Prime Minister’s Office Parcel B, the Malaysia Parliament, Imigresen Johor Bahru, Stadium DBKL Cheras, Garden Danga Bay, Forest City and etc.</p>

                            <!-- Blockquote -->
                            <blockquote class="confer-blockquote">
                                <h5>Lift & escalator designed to transport passengers up and down short vertical distances</h5>
                            </blockquote>

                            </br>

                            <h4>New Installation</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/e1.jpg" alt="">
                            </div>                  
                            <p>Our professional INSTALLATION company, leading new technology of installation is competitive with security, quality and efficiency in the industry.</p>

                            <br>

                            <h4>Modernization</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/e2.jfif" alt="">
                            </div>                  
                            <p>Regardless the condition of the building and type of your Vertical Transportation, our MODERNIZATION team will provide professional service of passenger space carrying solution for all areas which rich with experience and the latest technology from SWORD (M).</p>

                            <br>

                            <h4>Service and Repair</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/e1.jpg" alt="">
                            </div>                  
                            <p>Our SERVICE team will keep your Vertical Transportation running its best by providing high quality product and an outstanding service support to our customers 24 hours a day and 7 days a week.</p>

                            <br>

                            <h4>Maintenance</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/e1.jpg" alt="">
                            </div>
                            <p>Our MAINTENANCE team can tailor made your maintenance plan to uniquely match your site and equipment whatever the type or brand.</p>

                            <br>

                            
						</div>

                        <!-- Post Tags & Social Info -->
                        <div class="post-tags-social-area mt-30 pb-5 d-flex flex-wrap align-items-center">
                            <!-- Popular Tags -->
                            <div class="popular-tags d-flex align-items-center">
                                <p><i class="zmdi zmdi-label"></i></p>
                                <ul class="nav">
                                    <li><a href="#">Lift</a></li>
                                    <li><a href="#">Escalator</a></li>
                                </ul>
                            </div>

                            <!-- Author Social Info -->
                            <div class="author-social-info">
                                <a href="" ><i class="zmdi zmdi-facebook"></i></a>
								<a href="" ><i class="zmdi zmdi-linkedin"></i></a>                               
                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                            </div>
                        </div>

                        

                        
						</br>
                        <!-- Leave A Reply 
                        <div class="confer-leave-a-reply-form clearfix">
                            <h4 class="mb-30">Leave A Comment</h4>
                            
                            <!-- Leave A Reply 
                            <div class="contact_form">
                                <form action="#" method="post">
                                    <div class="contact_input_area">
                                        <div class="row">
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Your Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name-2" placeholder="Last Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="subject" id="subject" placeholder="Your Number">
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea name="message" class="form-control mb-30" id="message" cols="30" rows="6" placeholder="Message" required></textarea>
                                                </div>
                                            </div>
                                            <!-- Button 
                                            <div class="col-12">
                                                <button type="submit" class="btn confer-btn">Send Message <i class="zmdi zmdi-long-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>-->
                    </div>
                </div>

                
            </div>
        </div>
    </section>
    <!-- Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>