<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Learn and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business | Railway</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	
	<style>
	.hidden { display:none; }
	</style>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/hotspur-img/medical.jpeg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title">Railway</h2>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->

    <!-- Blog Area Start -->
    <section class="confer-blog-details-area section-padding-100-0">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Single Post Details Area -->
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="pr-lg-4 mb-100">
                        <!-- Post Content -->
                        <div class="post-details-content">

                           <!-- Post Thumbnail -->
                           <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/r0.jpeg" alt="">
                            </div>

                            <!-- Post Title -->
                            <h4 class="post-title">Moving platform that help us move from one floor to another floor</h4>

                            <!-- Post Meta -->
                            <div class="post-meta">
                                <a class="post-date"><i class="zmdi zmdi-alarm-check"></i> 2020</a>
                                <a class="post-author" href="" ><i class="zmdi zmdi-account"></i> Hotspur Sdn Bhd</a>
                                
                            </div>

                            <p>The government has earmarked the development and expansion of the rail industry as an important infrastructure project for Malaysia in the next 15 – 20 years. By strategically collaborating with leading organisations and companies in the areas of human capital development, education & training, supply and engineering maintenance, repair and overhaul (MRO) and system integration also not limited to testing and commissioning. Hotspur has taken an industrial lead forward. This includes delving into and undertaking both MRT and LRT projects. With the constant improving transportation network in Klang Valley, Hotspur is ecstatic to play a part in putting together a fully effective transport system for Malaysia.</p>
                            <p>Not only merely participate in the Rail project, Hotspur will set up a robust engineering team to surpass Malaysia’s name as a country exporting expertise abroad.</p>

                            <!-- Blockquote -->
                            <blockquote class="confer-blockquote">
                                <h5>Lift & escalator designed to transport passengers up and down short vertical distances</h5>
                            </blockquote>

                            </br>

                            <h4>MRT2</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/r1.jpg" alt="">
                            </div>                  
                            <p>We have secured a frontline system, Hotspur and Indra Systemas is a multinational company owned by the Government of Spain have collaborated to put together our expertise in engineering, procurement, construction, testing & commissioning of Automatic Fare Collection System.</p>

                            <br>

                            <h4>LRT3</h4>
                            <div class="post-blog-thumbnail mb-30">
                                <img src="img/hotspur-img/r2.jpg" alt="">
                            </div>                  
                            <p>We are both humbled and proud to be involved in the LRT3 project for a very important and vital system which is Signalling & Train Control System, Platform Screen Door System and Instrusion Preventive System with a triparty joint ventures combining Siemens Germany, Siemens Malaysia and Hotspur Sdn Bhd.</p>

                            <br>

						</div>

                        <!-- Post Tags & Social Info -->
                        <div class="post-tags-social-area mt-30 pb-5 d-flex flex-wrap align-items-center">
                            <!-- Popular Tags -->
                            <div class="popular-tags d-flex align-items-center">
                                <p><i class="zmdi zmdi-label"></i></p>
                                <ul class="nav">
                                    <li><a href="#">Railway</a></li>
                                </ul>
                            </div>

                            <!-- Author Social Info -->
                            <div class="author-social-info">
                                <a href="" ><i class="zmdi zmdi-facebook"></i></a>
								<a href="" ><i class="zmdi zmdi-linkedin"></i></a>                               
                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                            </div>
                        </div>

                        

                        
						</br>
                        <!-- Leave A Reply 
                        <div class="confer-leave-a-reply-form clearfix">
                            <h4 class="mb-30">Leave A Comment</h4>
                            
                            <!-- Leave A Reply 
                            <div class="contact_form">
                                <form action="#" method="post">
                                    <div class="contact_input_area">
                                        <div class="row">
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Your Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="name" id="name-2" placeholder="Last Name" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="email" class="form-control mb-30" name="email" id="email" placeholder="E-mail" required>
                                                </div>
                                            </div>
                                            <!-- Form Group
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control mb-30" name="subject" id="subject" placeholder="Your Number">
                                                </div>
                                            </div>
                                            <!-- Form Group 
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea name="message" class="form-control mb-30" id="message" cols="30" rows="6" placeholder="Message" required></textarea>
                                                </div>
                                            </div>
                                            <!-- Button 
                                            <div class="col-12">
                                                <button type="submit" class="btn confer-btn">Send Message <i class="zmdi zmdi-long-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>-->
                    </div>
                </div>

                
            </div>
        </div>
    </section>
    <!-- Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>