<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Visit our blog and start to digitalize your procurement process with our B2B e-procurement supply chain solutions in Malaysia.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Hotspur Sdn Bhd | Core Business</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/default3.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-141702531-1');
	</script>
</head>

<body>

    <!-- Header Area Start -->
    <?php include('inc/nav.php'); ?>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area2 bg-img bg-gradient-overlay jarallax" style="background-image: url(img/bg-img/blue1-01.jpg);">
        <div class="container h-100">
           
        </div>
    </section>
    <!-- Breadcrumb Area End -->

     <!-- ****** Top Catagory Area Start ****** -->
        <section class="top_catagory_area d-md-flex clearfix">
            <!-- Single Catagory -->
            <a href="electrical.php" class="m22 single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/elec.jpeg);">
				<div class="catagory-content">
                    <h2 class="hov01">Electrical Engineering</h2>
                </div>
            </a>
			
             <!-- Single Catagory -->
            <a href="mechanical.php" class="single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/mechy.jpeg);">
				<div class="catagory-content">
                    <h2 class="hov01">Mechanical Engineering</h2>
                </div>
            </a>
        </section>
        <!-- ****** Top Catagory Area End ****** -->
		 <!-- ****** Top Catagory Area Start ****** -->
        <section class="top_catagory_area d-md-flex clearfix">
            <!-- Single Catagory -->
            <a href="medical.php" class="single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/medic.jpeg);">
                <div class="catagory-content">
                    <h2 class="hov01">Medical Engineering</h2>
                </div>
            </a>
            <!-- Single Catagory -->
            <a href="lift-escalator.php" class="single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/esca.jpeg);">
                <div class="catagory-content">
                    <h2 class="hov01">Lift & Escalator</h2>
                </div>
            </a>
        </section>
        <!-- ****** Top Catagory Area End ****** -->
		 <!-- ****** Top Catagory Area Start ****** -->
        <section class="top_catagory_area d-md-flex clearfix">
            <!-- Single Catagory -->
            <a href="railway.php" class="single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/railway.jpeg);">
                <div class="catagory-content">
                    <h2 class="hov01">Railway</h2>
                </div>
            </a>
            <!-- Single Catagory -->
            <a href="training.php" class="single_catagory_area d-flex align-items-center bg-img" style="background-image: url(img/hotspur-img/equipment.jpeg);">
                <div class="catagory-content">
                    <h2 class="hov01">Training Equipment</h2>
                </div>
            </a>
        </section>
        <!-- ****** Top Catagory Area End ****** -->
		
    <!-- Our Blog Area End -->

    <!-- Footer Area Start -->
    <?php include('inc/footer.php'); ?>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/confer.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>